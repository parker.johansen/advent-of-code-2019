package part1

import (
	"strconv"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	var mass int
	for _, massString := range input {
		if len(massString) > 0 {
			mass, err = strconv.Atoi(massString)
			if err != nil {
				return
			}
			answer += GetFuel(mass)
		}
	}

	return
}

func GetFuel(mass int) int {
	return mass/3 - 2
}
