package part1

import (
	"strconv"
	"testing"
)

func TestPart1GetFuel(t *testing.T) {
	var inputs = []struct {
		input    int
		expected int
	}{
		{12, 2},
		{14, 2},
		{1969, 654},
		{100756, 33583},
	}
	for _, input := range inputs {
		t.Run(strconv.Itoa(input.input), func(t *testing.T) {
			if actual := GetFuel(input.input); actual != input.expected {
				t.Fatalf("expected: %d, actual: %d", input.expected, actual)
			}
		})
	}
}

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"12",
		"14",
		"1969",
		"100756",
	}
	expected := 34241
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		t.Error(err)
	}
	if actual != expected {
		t.Fatalf("expected: %d, actual: %d", expected, actual)
	}
}
