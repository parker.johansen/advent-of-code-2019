package part2

import (
	"strconv"
)

type Part2 struct{}

func (day Part2) Run(input []string) (answer int, err error) {
	var mass int
	for _, massString := range input {
		if len(massString) > 0 {
			mass, err = strconv.Atoi(massString)
			if err != nil {
				return
			}
			answer += GetFuel(mass)
		}
	}

	return
}

func GetFuel(mass int) int {
	fuel := mass/3 - 2
	if fuel <= 0 {
		return 0
	} else {
		return fuel + GetFuel(fuel)
	}
}
