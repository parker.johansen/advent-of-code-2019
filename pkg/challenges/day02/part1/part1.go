package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	var state []int
	stateStrs := strings.Split(input[0], ",")
	for _, str := range stateStrs {
		var i int
		i, err = strconv.Atoi(str)
		if err != nil {
			return
		}
		state = append(state, i)
	}

	state[1] = 12
	state[2] = 2

	state = changeState(0, state)
	answer = state[0]

	return
}

func changeState(startPos int, state []int) []int {
	opcode := state[startPos]
	if opcode == 99 {
		return state
	}

	pos1 := state[startPos+1]
	pos2 := state[startPos+2]
	changePos := state[startPos+3]
	if opcode == 1 {
		state[changePos] = state[pos1] + state[pos2]
	} else if opcode == 2 {
		state[changePos] = state[pos1] * state[pos2]
	}

	return changeState(startPos+4, state)
}
