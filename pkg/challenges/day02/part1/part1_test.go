package part1

import (
	"strconv"
	"testing"
)

func TestChangeState(t *testing.T) {
	var inputs = []struct {
		input    []int
		expected []int
	}{
		{[]int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50}, []int{3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50}},
		{[]int{1, 0, 0, 0, 99}, []int{2, 0, 0, 0, 99}},
		{[]int{2, 3, 0, 3, 99}, []int{2, 3, 0, 6, 99}},
		{[]int{2, 4, 4, 5, 99, 0}, []int{2, 4, 4, 5, 99, 9801}},
		{[]int{1, 1, 1, 4, 99, 5, 6, 0, 99}, []int{30, 1, 1, 4, 2, 5, 6, 0, 99}},
	}

	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual := changeState(0, input.input)
			for j, act := range actual {
				if act != expected[j] {
					t.Errorf("expected: %d, actual: %d", expected, actual)
				}
			}
		})
	}
}
