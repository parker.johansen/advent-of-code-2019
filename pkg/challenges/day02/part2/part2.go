package part2

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type Part2 struct{}

func resetState(stateStrs []string) (state []int, err error) {
	for _, str := range stateStrs {
		var i int
		i, err = strconv.Atoi(str)
		if err != nil {
			return
		}
		state = append(state, i)
	}

	return
}

func (part Part2) Run(input []string) (answer int, err error) {
	var state []int
	stateStrs := strings.Split(input[0], ",")

	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			state, err = resetState(stateStrs)
			if err != nil {
				return
			}
			state, ignore := RunWithInput(noun, verb, state)
			if ignore != nil {
				continue
			}
			if state[0] == 19690720 {
				answer = 100*noun + verb
				return
			}
		}
	}

	err = errors.New("not found")
	return
}

func RunWithInput(noun, verb int, state []int) ([]int, error) {
	state[1] = noun
	state[2] = verb
	return changeState(0, state)
}

func changeState(startPos int, state []int) ([]int, error) {
	opcode := state[startPos]
	if opcode == 99 {
		return state, nil
	}

	pos1 := state[startPos+1]
	pos2 := state[startPos+2]
	changePos := state[startPos+3]

	if pos1 >= len(state) || pos2 >= len(state) || changePos >= len(state) {
		return state, errors.New("index out of bounds")
	}
	if opcode == 1 {
		state[changePos] = state[pos1] + state[pos2]
	} else if opcode == 2 {
		state[changePos] = state[pos1] * state[pos2]
	} else {
		return state, fmt.Errorf("invalid op code %d", opcode)
	}

	return changeState(startPos+4, state)
}
