package part1

import (
	"math"
	"strconv"
	"strings"
)

type Part1 struct{}

type Position struct {
	x int
	y int
}

func (part Part1) Run(input []string) (answer int, err error) {
	wire1Movements := strings.Split(input[0], ",")
	wire1, err := CalculatePositions(Position{0, 0}, wire1Movements, []Position{})

	wire2Movements := strings.Split(input[1], ",")
	wire2, err := CalculatePositions(Position{0, 0}, wire2Movements, []Position{})

	commons := FindCommon(wire1, wire2)
	answer = ClosestCommon(commons)

	return
}

func ClosestCommon(pos []Position) (min int) {
	min = math.MaxInt64
	for _, p := range pos {
		distance := GetDistance(Position{0, 0}, p)
		if distance < min {
			min = distance
		}
	}
	return
}

func FindCommon(pos1, pos2 []Position) (common []Position) {
	for _, p1 := range pos1 {
		for _, p2 := range pos2 {
			if p1.x == p2.x && p1.y == p2.y && !(p1.x == 0 && p2.x == 0) {
				common = append(common, p1)
			}
		}
	}
	return
}

func CalculatePositions(pos Position, movements []string, acc []Position) ([]Position, error) {
	if len(movements) <= 0 {
		return acc, nil
	}

	newPos, err := Move(pos, movements[0])
	if err != nil {
		return acc, nil
	}
	acc = append(acc, newPos...)
	return CalculatePositions(newPos[len(newPos)-1], movements[1:], acc)
}

func GetDistance(pos1 Position, pos2 Position) int {
	return abs(pos1.x-pos2.x) + abs(pos1.y-pos2.y)
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func Move(pos Position, movement string) (newPos []Position, err error) {
	dir := movement[0:1]
	var distance int
	distance, err = strconv.Atoi(movement[1:])
	if err != nil {
		return
	}
	switch dir {
	case "U":
		for i := pos.y + 1; i <= pos.y+distance; i++ {
			newPos = append(newPos, Position{pos.x, i})
		}
	case "D":
		for i := pos.y - 1; i >= pos.y-distance; i-- {
			newPos = append(newPos, Position{pos.x, i})
		}
	case "L":
		for i := pos.x - 1; i >= pos.x-distance; i-- {
			newPos = append(newPos, Position{i, pos.y})
		}
	case "R":
		for i := pos.x + 1; i <= pos.x+distance; i++ {
			newPos = append(newPos, Position{i, pos.y})
		}
	}

	return
}
