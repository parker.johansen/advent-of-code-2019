package part1

import (
	"strconv"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []struct {
		wire1    string
		wire2    string
		expected int
	}{
		{"R8,U5,L5,D3", "U7,R6,D4,L4", 6},
		{"R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 159},
		{"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual, err := Part1{}.Run([]string{input.wire1, input.wire2})
			if err != nil {
				t.Errorf("error occurred: %v", err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestMove(t *testing.T) {
	var inputs = []struct {
		movement string
		expected []Position
	}{
		{"U2", []Position{
			{0, 1},
			{0, 2},
		}},
		{"D2", []Position{
			{0, -1},
			{0, -2},
		}},
		{"L2", []Position{
			{-1, 0},
			{-2, 0},
		}},
		{"R2", []Position{
			{1, 0},
			{2, 0},
		}},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			pos := Position{0, 0}
			actual, err := Move(pos, input.movement)
			if err != nil {
				t.Errorf("error occurred: %v", err)
			}
			for i, pos := range actual {
				if pos.x != input.expected[i].x || pos.y != input.expected[i].y {
					t.Fatalf("expected: %v, actual: %v", input.expected, actual)
				}
			}
		})
	}
}

func TestGetDistance(t *testing.T) {
	var inputs = []struct {
		pos1     Position
		pos2     Position
		expected int
	}{
		{Position{0, 0}, Position{3, 3}, 6},
		{Position{8, 15}, Position{40, 37}, 54},
		{Position{-40, -20}, Position{60, 20}, 140},
		{Position{-80, -40}, Position{-40, -80}, 80},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			actual := GetDistance(input.pos1, input.pos2)
			if actual != input.expected {
				t.Fatalf("expected: %d, actual: %d", input.expected, actual)
			}
		})
	}
}
