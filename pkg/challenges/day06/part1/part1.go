package part1

import "strings"

type Part1 struct{}

type Mode int

type Orbit struct {
	name     string
	children []*Orbit
}

type OrbitMap map[string]*Orbit

func (part Part1) Run(input []string) (answer int, err error) {
	orbitMap := parseOrbits(input)
	for _, orbit := range orbitMap {
		answer += orbit.dfsCount()
	}
	return
}

func (orbit Orbit) dfsCount() int {
	counter := len(orbit.children)
	for _, child := range orbit.children {
		counter += child.dfsCount()
	}
	return counter
}

func parseOrbits(input []string) OrbitMap {
	orbitMap := OrbitMap{}
	for _, in := range input {
		parts := strings.Split(in, ")")
		var orLeft, orRight *Orbit
		if val, ok := orbitMap[parts[0]]; ok {
			orLeft = val
		} else {
			orLeft = &Orbit{
				name:     parts[0],
				children: []*Orbit{},
			}
			orbitMap[parts[0]] = orLeft
		}
		if val, ok := orbitMap[parts[1]]; ok {
			orRight = val
		} else {
			orRight = &Orbit{
				name:     parts[1],
				children: []*Orbit{},
			}
			orbitMap[parts[1]] = orRight
		}
		orLeft.children = append(orLeft.children, orRight)
	}
	return orbitMap
}

func (orbit Orbit) containsChild(name string) bool {
	for _, k := range orbit.children {
		if k.name == name {
			return true
		}
	}
	return false
}
