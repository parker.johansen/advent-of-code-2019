package part1

import (
	"testing"
)

func TestPart1_Run(t *testing.T) {
	input := []string{
		"COM)B",
		"B)C",
		"C)D",
		"D)E",
		"E)F",
		"B)G",
		"G)H",
		"D)I",
		"E)J",
		"J)K",
		"K)L",
	}
	actual, err := Part1{}.Run(input)
	expected := 42
	if err != nil {
		t.Errorf("error thrown: %v", err)
	}
	if actual != expected {
		t.Errorf("expected: %d, actual: %d", expected, actual)
	}
}
