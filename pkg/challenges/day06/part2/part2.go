package part2

import (
	"math"
	"strings"
)

type Part2 struct{}

type Mode int

type Orbit struct {
	name     string
	parent   *Orbit
	children []*Orbit
}

type OrbitMap map[string]*Orbit

func (part Part2) Run(input []string) (answer int, err error) {
	orbitMap := parseOrbits(input)
	you := orbitMap["YOU"].parent
	san := orbitMap["SAN"].parent
	youParent := you.buildParentCount()
	sanParent := san.buildParentCount()
	answer = findCommonSum(youParent, sanParent)

	return
}

func findCommonSum(counts1, counts2 map[string]int) int {
	var commons []string
	for k := range counts1 {
		if _, ok := counts2[k]; ok {
			commons = append(commons, k)
		}
	}
	min := math.MaxInt64
	for _, com := range commons {
		if counts1[com]+counts2[com] < min {
			min = counts1[com] + counts2[com]
		}
	}
	return min
}

func (orbit Orbit) buildParentCount() (counts map[string]int) {
	counts = map[string]int{}
	parent := orbit.parent
	counter := 1
	for parent != nil {
		counts[parent.name] = counter
		parent = parent.parent
		counter++
	}
	return counts
}

func (orbit Orbit) dfs(name string) *Orbit {
	if orbit.name == name {
		return &orbit
	}
	for _, child := range orbit.children {
		d := child.dfs(name)
		if d != nil {
			return d
		}
	}
	return nil
}

func (orbit Orbit) dfsCount() int {
	counter := len(orbit.children)
	for _, child := range orbit.children {
		counter += child.dfsCount()
	}
	return counter
}

func parseOrbits(input []string) OrbitMap {
	orbitMap := OrbitMap{}
	for _, in := range input {
		parts := strings.Split(in, ")")
		var orLeft, orRight *Orbit
		if val, ok := orbitMap[parts[0]]; ok {
			orLeft = val
		} else {
			orLeft = &Orbit{
				name:     parts[0],
				parent:   nil,
				children: []*Orbit{},
			}
			orbitMap[parts[0]] = orLeft
		}
		if val, ok := orbitMap[parts[1]]; ok {
			orRight = val
		} else {
			orRight = &Orbit{
				name:     parts[1],
				parent:   nil,
				children: []*Orbit{},
			}
			orbitMap[parts[1]] = orRight
		}
		orRight.parent = orLeft
		orLeft.children = append(orLeft.children, orRight)
	}
	return orbitMap
}

func (orbit Orbit) containsChild(name string) bool {
	for _, k := range orbit.children {
		if k.name == name {
			return true
		}
	}
	return false
}
