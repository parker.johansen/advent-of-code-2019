package part1

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/utilities"
	"math"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	var state []int
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	perms := utilities.Permutations(0, 4)
	max := math.MinInt64
	for _, perm := range perms {
		channel := make(chan int)
		program := intcode.NewProgram(perm, state)
		program.SetFinalOutputConfiguration(&channel)
		go program.Run()
		out := <-channel
		close(channel)
		program.Halt()
		if out > max {
			max = out
		}
	}

	answer = max
	return
}
