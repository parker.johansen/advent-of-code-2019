package part1

import (
	"math"
	"strconv"
)

type Part1 struct{}

type Layer [][]int

func NewLayer(numRows, numCols int) *Layer {
	var lay Layer
	for i := 0; i < numRows; i++ {
		lay = append(lay, make([]int, numCols))
	}
	return &lay
}

func (part Part1) Run(input []string) (answer int, err error) {
	layers, err := parseLayers(input[0], 6, 25)
	if err != nil {
		return
	}

	minRow := getMinOf(0, layers)
	answer = getCountOf(1, minRow) * getCountOf(2, minRow)

	return
}

func getMinOf(num int, layers []*Layer) (minLayer *Layer) {
	min := math.MaxInt64
	for _, layer := range layers {
		count := getCountOf(num, layer)
		if count <= min {
			min = count
			minLayer = layer
		}
	}

	return
}

func getCountOf(num int, layer *Layer) (count int) {
	for _, row := range *layer {
		for _, col := range row {
			if col == num {
				count++
			}
		}
	}

	return
}

func parseLayers(input string, rowMax, colMax int) (layers []*Layer, err error) {
	layer := 0
	row := 0
	col := 0
	layers = append(layers, NewLayer(rowMax, colMax))
	for i, c := range input {
		(*layers[layer])[row][col], err = strconv.Atoi(string(c))
		if err != nil {
			return
		}
		col++
		if col >= colMax {
			col = 0
			row++
		}
		if row >= rowMax {
			row = 0
			col = 0
			layer++
			if i+1 != len(input) {
				layers = append(layers, NewLayer(rowMax, colMax))
			}
		}
	}

	return
}
