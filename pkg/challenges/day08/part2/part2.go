package part2

import (
	"fmt"
	"strconv"
)

type Part2 struct{}

type Layer [][]int

func NewLayer(numRows, numCols int) *Layer {
	var lay Layer
	for i := 0; i < numRows; i++ {
		lay = append(lay, make([]int, numCols))
	}
	return &lay
}

func (part Part2) Run(input []string) (answer int, err error) {
	layers, err := parseLayers(input[0], 6, 25)
	if err != nil {
		return
	}

	image := stackLayers(layers, 6, 25)

	printImage(image)

	return
}

func printImage(layer Layer) {
	for _, row := range layer {
		for _, col := range row {
			fmt.Print(col)
		}
		fmt.Println()
	}
}

func stackLayers(layers []*Layer, rowMax, colMax int) Layer {
	image := NewLayer(rowMax, colMax)
	for row := 0; row < rowMax; row++ {
		for col := 0; col < colMax; col++ {
			var pixel int
			for _, layer := range layers {
				pixel = (*layer)[row][col]
				if pixel != 2 {
					break
				}
			}
			(*image)[row][col] = pixel
		}
	}
	return *image
}

func parseLayers(input string, rowMax, colMax int) (layers []*Layer, err error) {
	layer := 0
	row := 0
	col := 0
	layers = append(layers, NewLayer(rowMax, colMax))
	for i, c := range input {
		(*layers[layer])[row][col], err = strconv.Atoi(string(c))
		if err != nil {
			return
		}
		col++
		if col >= colMax {
			col = 0
			row++
		}
		if row >= rowMax {
			row = 0
			col = 0
			layer++
			if i+1 != len(input) {
				layers = append(layers, NewLayer(rowMax, colMax))
			}
		}
	}

	return
}
