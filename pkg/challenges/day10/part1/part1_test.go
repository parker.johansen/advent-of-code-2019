package part1

import (
	"strconv"
	"testing"
)

var inputs = []struct {
	input    []string
	expected int
}{
	{[]string{
		".#..#",
		".....",
		"#####",
		"....#",
		"...##",
	}, 8},
	{[]string{
		"......#.#.",
		"#..#.#....",
		"..#######.",
		".#.#.###..",
		".#..#.....",
		"..#....#.#",
		"#..#....#.",
		".##.#..###",
		"##...#..#.",
		".#....####",
	}, 33},
	{[]string{
		"#.#...#.#.",
		".###....#.",
		".#....#...",
		"##.#.#.#.#",
		"....#.#.#.",
		".##..###.#",
		"..#...##..",
		"..##....##",
		"......#...",
		".####.###.",
	}, 35},
	{[]string{
		".#..#..###",
		"####.###.#",
		"....###.#.",
		"..###.##.#",
		"##.##.#.#.",
		"....###..#",
		"..#.#..#.#",
		"#..#.#.###",
		".##...##.#",
		".....#.#..",
	}, 41},
	{[]string{
		".#..##.###...#######",
		"##.############..##.",
		".#.######.########.#",
		".###.#######.####.#.",
		"#####.##.#.##.###.##",
		"..#####..#.#########",
		"####################",
		"#.####....###.#.#.##",
		"##.#################",
		"#####.##.###..####..",
		"..######..##.#######",
		"####.##.####...##..#",
		".#####..#.######.###",
		"##...#.##########...",
		"#.##########.#######",
		".####.#.###.###.#.##",
		"....##.##.###..#####",
		".#.#.###########.###",
		"#.#.#.#####.####.###",
		"###.##.####.##.#..##",
	}, 210},
}

func TestPart1_Run(t *testing.T) {
	t.Skip()
	for _, input := range inputs {
		t.Run(strconv.Itoa(input.expected), func(t *testing.T) {
			expected := input.expected
			actual, err := Part1{}.Run(input.input)
			if err != nil {
				t.Errorf("error: %v", err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
