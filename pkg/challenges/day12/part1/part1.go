package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

type Point struct {
	x, y, z int
}

type Position Point
type Velocity Point

type Moon struct {
	position *Position
	velocity *Velocity
}

type Orbit []*Moon

func (part Part1) Run(input []string) (answer int, err error) {
	orbit, err := parseMoons(input)
	if err != nil {
		return
	}
	for i := 0; i < 1000; i++ {
		orbit.Step()
	}

	answer = orbit.TotalEnergy()

	return
}

func (moon Moon) TotalEnergy() int {
	return moon.PotentialEnergy() * moon.KineticEnergy()
}

func (moon Moon) KineticEnergy() int {
	return abs(moon.velocity.x) + abs(moon.velocity.y) + abs(moon.velocity.z)
}

func (moon Moon) PotentialEnergy() int {
	return abs(moon.position.x) + abs(moon.position.y) + abs(moon.position.z)
}

func abs(val int) int {
	if val < 1 {
		return val * -1
	}

	return val
}

func (moons *Orbit) TotalEnergy() (total int) {
	for _, moon := range *moons {
		total += moon.TotalEnergy()
	}

	return
}

func (moons *Orbit) Step() {
	moons.CalculateGravity()
	moons.ApplyVelocity()
}

func (moons *Orbit) ApplyVelocity() {
	for _, moon := range *moons {
		moon.position.add(*moon.velocity)
	}
}

func (moons *Orbit) CalculateGravity() {
	var orbit Orbit
	orbit = append(orbit, *moons...)

	for i, moonI := range orbit {
		for j, moonJ := range orbit {
			if i == j {
				continue
			}
			change := moonI.getChange(*moonJ)
			(*moons)[i].velocity.add(change)
		}
	}
}

func (position *Position) add(other Velocity) {
	position.x += other.x
	position.y += other.y
	position.z += other.z
}

func (velocity *Velocity) add(other Point) {
	velocity.x += other.x
	velocity.y += other.y
	velocity.z += other.z
}

func (moon Moon) getChange(other Moon) (point Point) {
	if moon.position.x > other.position.x {
		point.x = -1
	} else if moon.position.x < other.position.x {
		point.x = 1
	}
	if moon.position.y > other.position.y {
		point.y = -1
	} else if moon.position.y < other.position.y {
		point.y = 1
	}
	if moon.position.z > other.position.z {
		point.z = -1
	} else if moon.position.z < other.position.z {
		point.z = 1
	}

	return
}

func parseMoons(input []string) (moons Orbit, err error) {
	for _, in := range input {
		in = in[1 : len(in)-1]
		parts := strings.Split(in, ", ")
		var x, y, z int
		x, err = strconv.Atoi(parts[0][2:])
		if err != nil {
			return
		}
		y, err = strconv.Atoi(parts[1][2:])
		if err != nil {
			return
		}
		z, err = strconv.Atoi(parts[2][2:])
		if err != nil {
			return
		}
		moon := Moon{
			position: &Position{x, y, z},
			velocity: &Velocity{0, 0, 0},
		}
		moons = append(moons, &moon)
	}

	return
}

func (moon Moon) equals(other Moon) bool {
	return moon.position.equals(*other.position) && moon.velocity.equals(*other.velocity)
}

func (position Position) equals(other Position) bool {
	return position.x == other.x && position.y == other.y && position.z == other.z
}

func (velocity Velocity) equals(other Velocity) bool {
	return velocity.x == other.x && velocity.y == other.y && velocity.z == other.z
}
