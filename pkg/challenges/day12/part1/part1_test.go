package part1

import (
	"strconv"
	"testing"
)

func TestPart1_Run(t *testing.T) {

}

func TestParseMoons(t *testing.T) {
	input := []string{
		"<x=-1, y=0, z=2>",
		"<x=2, y=-10, z=-7>",
		"<x=4, y=-8, z=8>",
		"<x=3, y=5, z=-1>",
	}
	expected := Orbit{
		{
			&Position{-1, 0, 2},
			&Velocity{0, 0, 0},
		}, {
			&Position{2, -10, -7},
			&Velocity{0, 0, 0},
		}, {
			&Position{4, -8, 8},
			&Velocity{0, 0, 0},
		}, {
			&Position{3, 5, -1},
			&Velocity{0, 0, 0},
		},
	}
	actual, err := parseMoons(input)
	for i, moon := range expected {
		if err != nil {
			t.Error(err)
		}
		if actualMoon := actual[i]; !actualMoon.equals(*moon) {
			t.Fatalf("expected: %v, actual %v", moon, actualMoon)
		}
	}
}

func TestOrbit_CalculateGravity(t *testing.T) {
	initial := Orbit{
		{
			&Position{-1, 0, 2},
			&Velocity{0, 0, 0},
		}, {
			&Position{2, -10, -7},
			&Velocity{0, 0, 0},
		}, {
			&Position{4, -8, 8},
			&Velocity{0, 0, 0},
		}, {
			&Position{3, 5, -1},
			&Velocity{0, 0, 0},
		},
	}
	expected := Orbit{
		{
			&Position{-1, 0, 2},
			&Velocity{3, -1, -1},
		}, {
			&Position{2, -10, -7},
			&Velocity{1, 3, 3},
		}, {
			&Position{4, -8, 8},
			&Velocity{-3, 1, -3},
		}, {
			&Position{3, 5, -1},
			&Velocity{-1, -3, 1},
		},
	}
	initial.CalculateGravity()
	actual := initial
	for i, moon := range expected {
		if actualMoon := actual[i]; !actualMoon.equals(*moon) {
			t.Fatalf("expected: %v, actual %v", moon, actualMoon)
		}
	}
}

func TestOrbit_ApplyVelocity(t *testing.T) {
	initial := Orbit{
		{
			&Position{-1, 0, 2},
			&Velocity{3, -1, -1},
		}, {
			&Position{2, -10, -7},
			&Velocity{1, 3, 3},
		}, {
			&Position{4, -8, 8},
			&Velocity{-3, 1, -3},
		}, {
			&Position{3, 5, -1},
			&Velocity{-1, -3, 1},
		},
	}
	expected := Orbit{
		{
			&Position{2, -1, 1},
			&Velocity{3, -1, -1},
		}, {
			&Position{3, -7, -4},
			&Velocity{1, 3, 3},
		}, {
			&Position{1, -7, 5},
			&Velocity{-3, 1, -3},
		}, {
			&Position{2, 2, 0},
			&Velocity{-1, -3, 1},
		},
	}
	initial.ApplyVelocity()
	actual := initial
	for i, moon := range expected {
		if actualMoon := actual[i]; !actualMoon.equals(*moon) {
			t.Fatalf("expected: %v, actual %v", moon, actualMoon)
		}
	}
}

func TestOrbit_Step(t *testing.T) {
	initial := Orbit{
		{
			&Position{-1, 0, 2},
			&Velocity{0, 0, 0},
		}, {
			&Position{2, -10, -7},
			&Velocity{0, 0, 0},
		}, {
			&Position{4, -8, 8},
			&Velocity{0, 0, 0},
		}, {
			&Position{3, 5, -1},
			&Velocity{0, 0, 0},
		},
	}
	expected := Orbit{
		{
			&Position{2, -1, 1},
			&Velocity{3, -1, -1},
		}, {
			&Position{3, -7, -4},
			&Velocity{1, 3, 3},
		}, {
			&Position{1, -7, 5},
			&Velocity{-3, 1, -3},
		}, {
			&Position{2, 2, 0},
			&Velocity{-1, -3, 1},
		},
	}
	initial.Step()
	actual := initial
	for i, moon := range expected {
		if actualMoon := actual[i]; !actualMoon.equals(*moon) {
			t.Fatalf("expected: %v, actual %v", moon, actualMoon)
		}
	}
}

var inputs = []struct {
	moon      Moon
	potential int
	kinetic   int
	total     int
}{
	{Moon{&Position{2, 1, -3}, &Velocity{-3, -2, 1}}, 6, 6, 36},
	{Moon{&Position{1, -8, 0}, &Velocity{-1, 1, 3}}, 9, 5, 45},
	{Moon{&Position{3, -6, 1}, &Velocity{3, 2, -3}}, 10, 8, 80},
	{Moon{&Position{2, 0, 4}, &Velocity{1, -1, -1}}, 6, 3, 18},
}

func TestMoon_PotentialEnergy(t *testing.T) {
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.potential
			actual := input.moon.PotentialEnergy()
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestMoon_KineticEnergy(t *testing.T) {
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.kinetic
			actual := input.moon.KineticEnergy()
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestMoon_TotalEnergy(t *testing.T) {
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.total
			actual := input.moon.TotalEnergy()
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}
