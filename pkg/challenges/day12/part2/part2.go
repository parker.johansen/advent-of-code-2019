package part2

import (
	"strconv"
	"strings"
)

type Part2 struct{}

type Point struct {
	x, y, z int
}

type Position Point
type Velocity Point

type Moon struct {
	position *Position
	velocity *Velocity
}

type Orbit []*Moon

func (part Part2) Run(input []string) (answer int, err error) {
	orbit, err := parseMoons(input)
	if err != nil {
		return
	}

	orbit.Step()

	var xStep, yStep, zStep int

	for step := 2; xStep == 0 || yStep == 0 || zStep == 0; step++ {
		orbit.Step()
		if xStep == 0 && orbit.all(func(moon Moon) bool { return moon.velocity.x == 0 }) {
			xStep = step
		}
		if yStep == 0 && orbit.all(func(moon Moon) bool { return moon.velocity.y == 0 }) {
			yStep = step
		}
		if zStep == 0 && orbit.all(func(moon Moon) bool { return moon.velocity.z == 0 }) {
			zStep = step
		}
	}

	answer = lcm(xStep, yStep, zStep)

	return
}

func PrimeFactors(n int) (factors []int) {
	for n%2 == 0 {
		factors = append(factors, 2)
		n = n / 2
	}

	for i := 3; i*i <= n; i = i + 2 {
		for n%i == 0 {
			factors = append(factors, i)
			n = n / i
		}
	}

	if n > 2 {
		factors = append(factors, n)
	}

	return
}

func group(ints ...int) (groups map[int]int) {
	groups = map[int]int{}
	for _, n := range ints {
		if _, ok := groups[n]; ok {
			groups[n]++
		} else {
			groups[n] = 1
		}
	}

	return
}

func maxGroups(groups ...map[int]int) (finalGroups map[int]int) {
	finalGroups = map[int]int{}
	for _, group := range groups {
		for k, v := range group {
			if fv, ok := finalGroups[k]; !ok || v > fv {
				finalGroups[k] = v
			}
		}
	}

	return
}

func lcm(x, y, z int) (total int) {
	xFactors := PrimeFactors(x)
	yFactors := PrimeFactors(y)
	zFactors := PrimeFactors(z)

	xGroups := group(xFactors...)
	yGroups := group(yFactors...)
	zGroups := group(zFactors...)

	finalGroups := maxGroups(xGroups, yGroups, zGroups)

	total = 1
	for k, v := range finalGroups {
		for i := 0; i < v; i++ {
			total *= k
		}
	}
	total *= 2

	return
}

type AllFunc func(Moon) bool

func (moons Orbit) all(allFunc AllFunc) bool {
	for _, moon := range moons {
		if !allFunc(*moon) {
			return false
		}
	}

	return true
}

func (moon Moon) TotalEnergy() int {
	return moon.PotentialEnergy() * moon.KineticEnergy()
}

func (moon Moon) KineticEnergy() int {
	return abs(moon.velocity.x) + abs(moon.velocity.y) + abs(moon.velocity.z)
}

func (moon Moon) PotentialEnergy() int {
	return abs(moon.position.x) + abs(moon.position.y) + abs(moon.position.z)
}

func abs(val int) int {
	if val < 1 {
		return val * -1
	}

	return val
}

func (moons *Orbit) TotalEnergy() (total int) {
	for _, moon := range *moons {
		total += moon.TotalEnergy()
	}

	return
}

func (moons *Orbit) Step() {
	moons.CalculateGravity()
	moons.ApplyVelocity()
}

func (moons *Orbit) ApplyVelocity() {
	for _, moon := range *moons {
		moon.position.add(*moon.velocity)
	}
}

func (moons *Orbit) CalculateGravity() {
	var orbit Orbit
	orbit = append(orbit, *moons...)

	for i, moonI := range orbit {
		for j, moonJ := range orbit {
			if i == j {
				continue
			}
			change := moonI.getChange(*moonJ)
			(*moons)[i].velocity.add(change)
		}
	}
}

func (position *Position) add(other Velocity) {
	position.x += other.x
	position.y += other.y
	position.z += other.z
}

func (velocity *Velocity) add(other Point) {
	velocity.x += other.x
	velocity.y += other.y
	velocity.z += other.z
}

func (moon Moon) getChange(other Moon) (point Point) {
	if moon.position.x > other.position.x {
		point.x = -1
	} else if moon.position.x < other.position.x {
		point.x = 1
	}
	if moon.position.y > other.position.y {
		point.y = -1
	} else if moon.position.y < other.position.y {
		point.y = 1
	}
	if moon.position.z > other.position.z {
		point.z = -1
	} else if moon.position.z < other.position.z {
		point.z = 1
	}

	return
}

func parseMoons(input []string) (moons Orbit, err error) {
	for _, in := range input {
		in = in[1 : len(in)-1]
		parts := strings.Split(in, ", ")
		var x, y, z int
		x, err = strconv.Atoi(parts[0][2:])
		if err != nil {
			return
		}
		y, err = strconv.Atoi(parts[1][2:])
		if err != nil {
			return
		}
		z, err = strconv.Atoi(parts[2][2:])
		if err != nil {
			return
		}
		moon := Moon{
			position: &Position{x, y, z},
			velocity: &Velocity{0, 0, 0},
		}
		moons = append(moons, &moon)
	}

	return
}

func (moon Moon) equals(other Moon) bool {
	return moon.position.equals(*other.position) && moon.velocity.equals(*other.velocity)
}

func (position Position) equals(other Position) bool {
	return position.x == other.x && position.y == other.y && position.z == other.z
}

func (velocity Velocity) equals(other Velocity) bool {
	return velocity.x == other.x && velocity.y == other.y && velocity.z == other.z
}

func (moons Orbit) equals(other Orbit) bool {
	for i, moon := range moons {
		if otherMoon := other[i]; !otherMoon.equals(*moon) {
			return false
		}
	}

	return true
}
