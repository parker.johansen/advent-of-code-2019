package part2

import (
	"fmt"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"io/ioutil"
	"strconv"
	"strings"
	"sync"
)

type Part2 struct{}

type TileType int

const (
	Empty      TileType = iota
	Wall       TileType = iota
	Block      TileType = iota
	Ball       TileType = iota
	Horizontal TileType = iota
)

type Tile struct {
	x, y     int
	tileType TileType
}

type Game struct {
	grid      *Grid
	player    *Tile
	ball      *Tile
	lastBall  *Tile
	program   *intcode.Program
	channel   *chan int
	inChannel *chan int
}

type Grid map[string]*Tile

func (grid Grid) print() {
	for i := 0; i < 1000; i++ {
		fmt.Println()
	}

	var array [][]Tile
	for _, v := range grid {
		row := v.y
		col := v.x
		if len(array) <= row {
			for i := len(array); i <= row; i++ {
				array = append(array, []Tile{})
			}
		}
		if len(array[row]) <= col {
			for i := len(array[row]); i <= col; i++ {
				array[row] = append(array[row], Tile{row, col, Empty})
			}
		}
		array[row][col] = *v
	}

	for _, row := range array {
		for _, col := range row {
			col.print()
		}
		fmt.Println()
	}

	//time.Sleep(1000 * time.Millisecond)
}

func NewGame(state intcode.State) Game {
	state[0] = 2

	channel := make(chan int)
	program := intcode.NewProgram([]int{1}, state)
	program.SetFinalOutputConfiguration(&channel)
	program.Computers[0].SetSilent()
	program.Computers[0].SetInteractive()

	savedInput, _ := ioutil.ReadFile("inputs/interactive.txt")
	inputStrings := strings.Split(string(savedInput), "\n")
	go func() {
		for _, str := range inputStrings {
			if str != "" {
				i, _ := strconv.Atoi(str)
				*program.Computers[0].InChannel <- i
			}
		}
		close(*program.Computers[0].InChannel)
	}()

	tiles := Grid{}
	game := Game{
		grid:      &tiles,
		player:    nil,
		ball:      nil,
		program:   &program,
		channel:   &channel,
		inChannel: program.Computers[0].InChannel,
	}

	return game
}

func (game Game) Run() (answer int) {
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		var x, y, tileId int
		index := 0
		for val := range *game.channel {
			//game.setInput()
			if index == 0 {
				x = val
				index++
			} else if index == 1 {
				y = val
				index++
			} else {
				tileId = val
				if x == -1 && y == 0 {
					answer = tileId
					fmt.Printf("Score: %d", answer)
				} else {
					tile := Tile{
						x,
						y,
						ItoTileType(tileId),
					}
					(*game.grid)[xyToString(x, y)] = &tile
					if tile.tileType == Ball {
						if game.ball != nil {
							game.lastBall = game.ball
						}
						game.ball = &tile
					}
					if tile.tileType == Horizontal {
						game.player = &tile
					}
				}
				index = 0
				game.grid.print()
			}
		}
		wg.Done()
	}()
	game.program.Run()
	game.program.Close()
	close(*game.channel)
	wg.Wait()

	return
}

func (game Game) setInput() {
	if game.lastBall == nil || game.ball == nil || game.player == nil {
		return
	}

	if game.ball.x < game.player.x {
		setInput(game.inChannel, -1)
	} else if game.ball.x > game.player.x {
		setInput(game.inChannel, 1)
	} else if game.lastBall.x < game.ball.x {
		setInput(game.inChannel, 1)
	} else if game.lastBall.x > game.ball.x {
		setInput(game.inChannel, -1)
	} else {
		setInput(game.inChannel, 0)
	}
}

func setInput(channel *chan int, val int) {
	for len(*channel) > 0 {
		<-*channel
	}
	*channel <- val
}

func (part Part2) Run(input []string) (answer int, err error) {
	var state intcode.State
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	game := NewGame(state)
	answer = game.Run()

	return
}

func xyToString(x, y int) string {
	return strconv.Itoa(x) + "," + strconv.Itoa(y)
}

func ItoTileType(i int) TileType {
	//0 is an empty tile. No game object appears in this tile.
	//1 is a wall tile. Walls are indestructible barriers.
	//2 is a block tile. Blocks can be broken by the ball.
	//3 is a horizontal paddle tile. The paddle is indestructible.
	//4 is a ball tile. The ball moves diag

	switch i {
	case 0:
		return Empty
	case 1:
		return Wall
	case 2:
		return Block
	case 3:
		return Horizontal
	case 4:
		return Ball
	default:
		return Empty
	}
}

func (tile Tile) print() {
	switch tile.tileType {
	case Empty:
		fmt.Print(" ")
	case Wall:
		fmt.Print("|")
	case Block:
		fmt.Print("*")
	case Horizontal:
		fmt.Print("_")
	case Ball:
		fmt.Print("@")
	}
}
