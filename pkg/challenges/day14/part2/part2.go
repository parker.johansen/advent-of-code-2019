package part2

import (
	"fmt"
	"strconv"
	"strings"
)

type Part2 struct{}

type Resource struct {
	name   string
	amount int
}

type Requirement struct {
	resource Resource
	deps     []Resource
}

type ReqMap map[string]Requirement

func (part Part2) Run(input []string) (answer int, err error) {
	reqs, err := parseInput(input)
	if err != nil {
		return
	}

	amount := 10000000
	fuel := 1
	for {
		ore := 0
		for ore <= 1000000000000 {
			ore = getOre(Resource{"FUEL", fuel}, reqs, &map[string]int{})
			fuel += amount
		}
		if amount <= 1 {
			answer = fuel - 2
			return
		}
		fuel -= amount * 2

		amount /= 10
	}
}

type MapQueue struct {
	values   map[string]Resource
	keyQueue []string
}

func NewMapQueue() (queue MapQueue) {
	queue.values = map[string]Resource{}
	return
}

func (queue *MapQueue) pop() (value Resource) {
	key := queue.keyQueue[0]
	queue.keyQueue = queue.keyQueue[1:]
	value = queue.values[key]
	delete(queue.values, key)

	return
}

func (queue *MapQueue) push(values ...Resource) {
	for _, value := range values {
		if val, ok := (queue.values)[value.name]; !ok {
			value.amount += val.amount
		}
		(queue.values)[value.name] = value
		queue.keyQueue = append(queue.keyQueue, value.name)
	}
}

func getOre(resource Resource, reqMap ReqMap, available *map[string]int) (total int) {
	queue := []Resource{resource}
	for len(queue) > 0 {
		pop := queue[0]
		queue = queue[1:]
		if pop.name == "ORE" {
			total += pop.amount
			continue
		}
		tryFromAvailable(&pop, available)
		if pop.amount > 0 {
			requirements := reqMap[pop.name]
			waste, deps := convert(pop.amount, requirements)
			queue = append(queue, deps...)
			if _, ok := (*available)[waste.name]; !ok {
				(*available)[waste.name] = 0
			}
			(*available)[waste.name] += waste.amount
		}
	}

	(*available)["ORE"] -= total

	return
}

//func getOre(resource Resource, reqMap ReqMap, available *map[string]int) (total int) {
//	var final []Resource
//	queue := NewMapQueue()
//	queue.push(resource)
//	for len(queue.keyQueue) > 0 {
//		pop := queue.pop()
//		if pop.name == "ORE" {
//			final = append(final, pop)
//			continue
//		}
//		tryFromAvailable(&pop, available)
//		if pop.amount > 0 {
//			requirements := reqMap[pop.name]
//			waste, deps := convert(pop.amount, requirements)
//			queue.push(deps...)
//			if _, ok := (*available)[waste.name]; !ok {
//				(*available)[waste.name] = 0
//			}
//			(*available)[waste.name] += waste.amount
//		}
//	}
//	for _, res := range final {
//		total += res.amount
//	}
//	(*available)["ORE"] -= total
//
//	return
//}

func tryFromAvailable(resource *Resource, available *map[string]int) {
	if availableAmount, ok := (*available)[resource.name]; ok {
		if resource.amount >= availableAmount {
			resource.amount -= availableAmount
			delete(*available, resource.name)
		} else {
			(*available)[resource.name] -= resource.amount
			resource.amount = 0
		}

	}
}

func convert(amount int, req Requirement) (waste Resource, resources []Resource) {
	repetitions := amount / req.resource.amount
	if amount%req.resource.amount != 0 {
		repetitions += 1
	}
	for _, dep := range req.deps {
		resources = append(resources, Resource{dep.name, dep.amount * repetitions})
	}

	if created := repetitions * req.resource.amount; created > amount {
		waste = Resource{req.resource.name, created - amount}
	}

	return
}

func parseInput(input []string) (reqs ReqMap, err error) {
	reqs = ReqMap{}
	for _, line := range input {
		inputs, output, err1 := parseLine(line)
		if err1 != nil {
			return nil, err1
		}
		req := Requirement{
			resource: output,
			deps:     nil,
		}
		for _, in := range inputs {
			req.deps = append(req.deps, in)
		}
		reqs[output.name] = req
	}

	return
}

func parseLine(line string) (inputs []Resource, output Resource, err error) {
	insOut := strings.Split(line, " => ")
	ins := strings.Split(insOut[0], ", ")
	for _, in := range ins {
		resource, err1 := newResource(in)
		if err1 != nil {
			return inputs, output, fmt.Errorf("unable to parse resource: '%s'. error: %v", in, err1)
		}
		inputs = append(inputs, resource)
	}
	output, err = newResource(insOut[1])

	return
}

func newResource(resourceStr string) (resource Resource, err error) {
	parts := strings.Split(resourceStr, " ")
	amount, err := strconv.Atoi(parts[0])
	if err != nil {
		return
	}
	name := parts[1]
	resource = Resource{
		name,
		amount,
	}

	return
}
