package part1

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	inStrs := strings.Split(input[0], "")
	var in []int
	for _, inStr := range inStrs {
		var num int
		num, err = strconv.Atoi(inStr)
		if err != nil {
			return
		}
		in = append(in, num)
	}

	for i := 0; i < 100; i++ {
		in, err = phase(in)
		if err != nil {
			return
		}
		if (i+1)%10 == 0 {
			fmt.Printf("Completed phase '%d'\n", i+1)
		}
	}

	var str string
	for _, n := range in[:8] {
		str += strconv.Itoa(n)
	}

	answer, err = strconv.Atoi(str)

	return
}

func phase(input []int) ([]int, error) {
	//var out []int
	out := make([]int, len(input))
	var wg sync.WaitGroup
	for i := range input {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			row, _ := calcRow(index+1, input)
			out[index] = row
		}(i)
	}
	wg.Wait()

	return out, nil
}

func calcRow(index int, input []int) (int, error) {
	pattern := getPattern(index, len(input))
	sum := 0
	for j, in := range input {
		sum += in * pattern[j]
	}
	return trim(sum)
}

func trim(input int) (int, error) {
	full := strconv.Itoa(input)
	return strconv.Atoi(full[len(full)-1:])
}

func getPattern(index, length int) (pattern []int) {
	basePattern := []int{0, 1, 0, -1}
	for patternLen := 0; patternLen < length+1; patternLen++ {
		for i := 0; i < index; i++ {
			nextIndex := patternLen % len(basePattern)
			pattern = append(pattern, basePattern[nextIndex])
		}
	}

	pattern = pattern[1 : length+1]

	return
}
