package part1

import (
	"strconv"
	"testing"
)

func TestPart1_Run(t *testing.T) {
	inputs := []struct {
		input    string
		expected int
	}{
		{"80871224585914546619083218645595", 24176176},
		{"19617804207202209144916044189917", 73745418},
		{"69317163492948606335995924319873", 52432133},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual, err := Part1{}.Run([]string{input.input})
			if err != nil {
				t.Error(err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestGetPattern(t *testing.T) {
	inputs := []struct {
		index    int
		expected []int
	}{
		{
			1,
			[]int{1, 0, -1},
		}, {
			1,
			[]int{1, 0, -1, 0},
		}, {
			2,
			[]int{0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1},
		}, {
			3,
			[]int{0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1},
		},
	}
	for _, input := range inputs {
		t.Run(strconv.Itoa(input.index), func(t *testing.T) {
			expected := input.expected
			actual := getPattern(input.index, len(input.expected))
			for i := range input.expected {
				if actual[i] != expected[i] {
					t.Fatalf("expected: %v, actual: %v", expected, actual)
				}
			}
		})
	}
}

func TestPhase(t *testing.T) {
	inputs := []struct {
		input    []int
		expected []int
	}{
		{
			[]int{1, 2, 3, 4, 5, 6, 7, 8},
			[]int{4, 8, 2, 2, 6, 1, 5, 8},
		}, {
			[]int{4, 8, 2, 2, 6, 1, 5, 8},
			[]int{3, 4, 0, 4, 0, 4, 3, 8},
		}, {
			[]int{3, 4, 0, 4, 0, 4, 3, 8},
			[]int{0, 3, 4, 1, 5, 5, 1, 8},
		}, {
			[]int{0, 3, 4, 1, 5, 5, 1, 8},
			[]int{0, 1, 0, 2, 9, 4, 9, 8},
		},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual, err := phase(input.input)
			if err != nil {
				t.Error(err)
			}
			for i := range input.expected {
				if actual[i] != expected[i] {
					t.Fatalf("expected: %v, actual: %v", expected, actual)
				}
			}
		})
	}
}
