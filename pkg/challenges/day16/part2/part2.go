package part2

import (
	"fmt"
	"strconv"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	inStrs := strings.Split(input[0], "")

	var skip int
	skip, err = strconv.Atoi(input[0][:7])
	if err != nil {
		return
	}

	var in []int
	for _, inStr := range inStrs {
		var num int
		num, err = strconv.Atoi(inStr)
		if err != nil {
			return
		}
		in = append(in, num)
	}

	newLen := (len(in) * 10000) - skip
	start := skip % len(in)

	newIn := in[start+1:]
	for i := start; i <= newLen; i++ {
		newIn = append(in, in...)
	}

	in = newIn

	for i := 0; i < 100; i++ {
		in, err = phase(in, nil, skip)
		if err != nil {
			return
		}
		if (i+1)%10 == 0 {
			fmt.Printf("Completed phase '%d'\n", i+1)
		}
	}

	var str string
	for _, n := range in[:8] {
		str += strconv.Itoa(n)
	}

	answer, err = strconv.Atoi(str)

	return
}

func phase(input []int, pattern []int, offset int) ([]int, error) {
	out := make([]int, len(input))
	//out[len(input)-1] = 8
	for i := range input {
		//for i := len(input) - 2; i >= 0; i-- {
		//	val := out[i+1]
		//	out[i] = calcRow(i, val, input)
		out[i] = calcRow(i+1, input, pattern, offset)
	}

	return out, nil
}

//func calcRow(index, indexPlusOneVal int, input []int) int {
func calcRow(index int, input []int, pattern []int, offset int) int {
	if pattern == nil {
		pattern = getPattern(index, len(input)+offset)
		pattern = pattern[offset:]
	}
	sum := 0
	for j, in := range input {
		sum += in * pattern[j]
	}
	return trim(sum)

	//return trim(input[index] + indexPlusOneVal)
}

func trim(input int) int {
	x := input % 10
	if x < 1 {
		return x * -1
	}

	return x
	//full := strconv.Itoa(input)
	//return strconv.Atoi(full[len(full)-1:])
}

var patterns map[string][]int = map[string][]int{}

func getPattern(index, length int) (pattern []int) {
	patternKey := strconv.Itoa(index) + strconv.Itoa(length)
	if pattern, ok := patterns[patternKey]; ok {
		return pattern
	}

	basePattern := []int{0, 1, 0, -1}
	for patternLen := 0; patternLen < length+1; patternLen++ {
		for i := 0; i < index; i++ {
			nextIndex := patternLen % len(basePattern)
			pattern = append(pattern, basePattern[nextIndex])
		}
	}

	pattern = pattern[1 : length+1]
	patterns[patternKey] = pattern

	return
}
