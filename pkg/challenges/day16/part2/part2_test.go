package part2

import (
	"strconv"
	"testing"
)

func TestPart2_Run(t *testing.T) {
	t.Skip()
	inputs := []struct {
		input    string
		expected int
	}{
		{"03036732577212944063491565474664", 84462026},
		{"02935109699940807407585447034323", 78725270},
		{"03081770884921959731165446850517", 53553731},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual, err := Part2{}.Run([]string{input.input})
			if err != nil {
				t.Error(err)
			}
			if actual != expected {
				t.Fatalf("expected: %d, actual: %d", expected, actual)
			}
		})
	}
}

func TestGetPattern(t *testing.T) {
	inputs := []struct {
		index    int
		expected []int
	}{
		{
			1,
			[]int{1, 0, -1},
		}, {
			1,
			[]int{1, 0, -1, 0},
		}, {
			2,
			[]int{0, 1, 1, 0, 0, -1, -1, 0, 0, 1, 1, 0, 0, -1, -1},
		}, {
			3,
			[]int{0, 0, 1, 1, 1, 0, 0, 0, -1, -1, -1},
		},
	}
	for _, input := range inputs {
		t.Run(strconv.Itoa(input.index), func(t *testing.T) {
			expected := input.expected
			actual := getPattern(input.index, len(input.expected))
			for i := range input.expected {
				if actual[i] != expected[i] {
					t.Fatalf("expected: %v, actual: %v", expected, actual)
				}
			}
		})
	}
}

func TestPhase(t *testing.T) {
	inputs := []struct {
		input    []int
		expected []int
	}{
		{
			[]int{1, 2, 3, 4, 5, 6, 7, 8},
			[]int{4, 8, 2, 2, 6, 1, 5, 8},
		}, {
			[]int{4, 8, 2, 2, 6, 1, 5, 8},
			[]int{3, 4, 0, 4, 0, 4, 3, 8},
		}, {
			[]int{3, 4, 0, 4, 0, 4, 3, 8},
			[]int{0, 3, 4, 1, 5, 5, 1, 8},
		}, {
			[]int{0, 3, 4, 1, 5, 5, 1, 8},
			[]int{0, 1, 0, 2, 9, 4, 9, 8},
		},
	}
	for i, input := range inputs {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := input.expected
			actual, err := phase(input.input, nil, 0)
			if err != nil {
				t.Error(err)
			}
			for i := range input.expected {
				if actual[i] != expected[i] {
					t.Fatalf("expected: %v, actual: %v", expected, actual)
				}
			}
		})
	}
}
