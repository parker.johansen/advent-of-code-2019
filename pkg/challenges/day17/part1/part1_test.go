package part1

import (
	"strings"
	"testing"
)

func TestIsIntersection(t *testing.T) {
	tests := []struct {
		karta    []string
		expected bool
	}{
		{
			[]string{
				".#.",
				"###",
				".#.",
			}, true,
		},
		{
			[]string{
				".#.",
				"#.#",
				".#.",
			}, false,
		},
		{
			[]string{
				"...",
				"###",
				".#.",
			}, false,
		},
		{
			[]string{
				".#.",
				".##",
				".#.",
			}, false,
		},
		{
			[]string{
				".#.",
				"##.",
				".#.",
			}, false,
		},
		{
			[]string{
				".#.",
				"###",
				"...",
			}, false,
		},
	}

	for _, test := range tests {
		t.Run(strings.Join(test.karta, "\n"), func(t *testing.T) {
			expected := test.expected
			actual := IsIntersection(1, 1, test.karta)
			if actual != expected {
				t.Errorf("expected: %t, actual %t", expected, actual)
			}
		})
	}
}
