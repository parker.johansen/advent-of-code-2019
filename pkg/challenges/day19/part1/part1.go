package part1

import (
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	var state []int
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	for y := 0; y < 50; y++ {
		for x := 0; x < 50; x++ {
			outChannel := make(chan int, 1)
			program := intcode.NewProgram([]int{x}, state)
			program.SetFinalOutputConfiguration(&outChannel)
			inChannel := program.Computers[0].InChannel
			*inChannel <- y
			program.Run()
			answer += <-outChannel
			program.Close()
		}
	}

	return
}
