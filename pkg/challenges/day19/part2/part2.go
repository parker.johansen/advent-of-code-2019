package part2

import (
	"fmt"
	"gitlab.com/parker.johansen/advent-of-code-2019/pkg/intcode"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	var state []int
	stateStrs := strings.Split(input[0], ",")

	state, err = intcode.NewState(stateStrs)
	if err != nil {
		return
	}

	x := 0
	y := 20
	for ; ; y++ {
		for !inBeam(x, y, state) {
			x++
		}
		fmt.Printf("y: %d, x: %d\n", x, y)
		if inBeam(x, y, state) &&
			inBeam(x, y-99, state) &&
			inBeam(x+99, y, state) &&
			inBeam(x+99, y-99, state) {
			break
		}
	}

	answer = (x * 10000) + (y - 99)

	return
}

func inBeam(x, y int, state intcode.State) bool {
	outChannel := make(chan int, 1)
	program := intcode.NewProgram([]int{x}, state)
	program.SetFinalOutputConfiguration(&outChannel)
	program.Computers[0].SetSilent()
	inChannel := program.Computers[0].InChannel
	*inChannel <- y
	program.Run()
	out := <-outChannel
	program.Close()

	return out == 1
}
