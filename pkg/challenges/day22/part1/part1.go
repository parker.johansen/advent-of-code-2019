package part1

import (
	"fmt"
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	cards, err := Shuffle(input, 10007)
	if err != nil {
		return
	}

	for i, val := range cards {
		if val == 2019 {
			answer = i
			break
		}
	}

	return
}

func Shuffle(process []string, length int) (newStack []int, err error) {
	for i := 0; i < length; i++ {
		newStack = append(newStack, i)
	}

	for _, step := range process {
		command, input, err := ParseCommand(step, len(newStack))
		if err != nil {
			return nil, err
		}
		newStack = command(newStack, input)
	}

	return
}

func ParseCommand(command string, length int) (function func([]int, int) []int, n int, err error) {
	if strings.Contains(command, "deal with increment") {
		function = DealIncrement
		n, err = strconv.Atoi(command[20:])
	} else if strings.Contains(command, "deal into new stack") {
		function = DealIntoNewStack
		n = length
	} else if strings.Contains(command, "cut") {
		function = CutN
		n, err = strconv.Atoi(command[4:])
	} else {
		return nil, 0, fmt.Errorf("unknown command '%s'", command)
	}

	return
}

func DealIntoNewStack(stack []int, length int) (newStack []int) {
	for i := length - 1; i >= 0; i-- {
		newStack = append(newStack, stack[i])
	}

	return
}

func CutN(stack []int, n int) []int {
	var cut, rest []int
	if n >= 0 {
		cut = stack[:n]
		rest = stack[n:]
	} else {
		cut = stack[:len(stack)+n]
		rest = stack[len(stack)+n:]
	}

	return append(rest, cut...)
}

func DealIncrement(stack []int, n int) (newStack []int) {
	for _ = range stack {
		newStack = append(newStack, -1)
	}

	i := 0
	for _, val := range stack {
		newStack[i] = val
		i = (i + n) % len(stack)
	}

	return
}
