package intcode

import (
	"fmt"
	term "github.com/nsf/termbox-go"
	"os"
	"strconv"
)

type Computer struct {
	number       int
	phase        int
	state        State
	position     int
	relativeBase int
	phaseSet     bool
	halted       bool
	interactive  bool
	silent       bool
	Idle         bool
	InChannel    *chan int
	OutChannel   *chan int
	haltChannel  *chan struct{}
}

func (computer *Computer) setOutChannel(channel *chan int) {
	computer.OutChannel = channel
}

func (computer *Computer) SetInteractive() {
	computer.interactive = true
}

func (computer *Computer) SetSilent() {
	computer.silent = true
}

func NewComputer(number int, phase int, initialState State) Computer {
	channel := make(chan int, 1000)
	haltChannel := make(chan struct{}, 2)
	return Computer{
		number,
		phase,
		append(initialState[:0:0], initialState...),
		0,
		0,
		false,
		false,
		false,
		false,
		false,
		&channel,
		nil,
		&haltChannel,
	}
}

func (computer *Computer) Run() error {
	computer.message("Starting up!")
	for !computer.halted {
		opcode, err := parseOpcode(computer.state.GetValue(computer.position))
		if err != nil {
			return err
		}
		err = computer.change(opcode)
		if err != nil {
			return err
		}
	}

	return nil
}

func (computer *Computer) Halt() {
	*computer.haltChannel <- struct{}{}
}

func (computer *Computer) change(opcode Opcode) error {
	group := opcode.modeGroup

	switch opcode.code {
	case 1:
		computer.code1(group)
	case 2:
		computer.code2(group)
	case 3:
		computer.code3(group)
	case 4:
		computer.code4(group)
	case 5:
		computer.code5(group)
	case 6:
		computer.code6(group)
	case 7:
		computer.code7(group)
	case 8:
		computer.code8(group)
	case 9:
		computer.code9(group)
	case 99:
		computer.codeHalt()
	default:
		return fmt.Errorf("invalid op code %d\n", opcode.code)
	}
	return nil
}

func (computer *Computer) code1(group ModeGroup) {
	changePos := computer.state.GetValue(computer.position + 3)
	val1 := group[0].GetVal(computer.position+1, computer)
	val2 := group[1].GetVal(computer.position+2, computer)
	group[2].SetVal(changePos, computer, val1+val2)
	computer.position += 4
}

func (computer *Computer) code2(group ModeGroup) {
	changePos := computer.state.GetValue(computer.position + 3)
	val1 := group[0].GetVal(computer.position+1, computer)
	val2 := group[1].GetVal(computer.position+2, computer)
	group[2].SetVal(changePos, computer, val1*val2)
	computer.position += 4
}

func (computer *Computer) code3(group ModeGroup) {
	var input int
	if computer.interactive {
		if len(*computer.InChannel) != 0 {
			input = <-*computer.InChannel
		} else {
			input = computer.getInput()
			f, err := os.OpenFile("inputs/interactive.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0666)
			if err != nil {
				panic(err)
			}
			defer f.Close()
			if _, err = f.WriteString(strconv.Itoa(input) + "\n"); err != nil {
				panic(err)
			}
		}
	} else if computer.phaseSet {
		computer.message("Waiting for input")
		if len(*computer.InChannel) <= 0 {
			input = -1
			computer.Idle = true
		} else {
			computer.Idle = false
			select {
			case input = <-*computer.InChannel:
				computer.message("Received '%d' from input channel\n", input)
			case <-*computer.haltChannel:
				computer.halted = true
			}
		}
	} else {
		input = computer.phase
		computer.phaseSet = true
		computer.message("Received phase setting '%d'\n", input)
	}
	changePos := computer.state.GetValue(computer.position + 1)
	//computer.state.SetValue(changePos, input)
	group[0].SetVal(changePos, computer, input)
	computer.position += 2
}

func (computer *Computer) code4(group ModeGroup) {
	val := group[0].GetVal(computer.position+1, computer)
	computer.message("Sending '%d' to out channel\n", val)
	*computer.OutChannel <- val
	computer.position += 2
}

func (computer *Computer) code5(group ModeGroup) {
	val1 := group[0].GetVal(computer.position+1, computer)
	val2 := group[1].GetVal(computer.position+2, computer)
	if val1 != 0 {
		computer.position = val2
	} else {
		computer.position += 3
	}
}

func (computer *Computer) code6(group ModeGroup) {
	val1 := group[0].GetVal(computer.position+1, computer)
	val2 := group[1].GetVal(computer.position+2, computer)
	if val1 == 0 {
		computer.position = val2
	} else {
		computer.position += 3
	}
}

func (computer *Computer) code7(group ModeGroup) {
	changePos := computer.state.GetValue(computer.position + 3)
	val1 := group[0].GetVal(computer.position+1, computer)
	val2 := group[1].GetVal(computer.position+2, computer)
	if val1 < val2 {
		group[2].SetVal(changePos, computer, 1)
	} else {
		group[2].SetVal(changePos, computer, 0)
	}
	computer.position += 4
}

func (computer *Computer) code8(group ModeGroup) {
	changePos := computer.state.GetValue(computer.position + 3)
	val1 := group[0].GetVal(computer.position+1, computer)
	val2 := group[1].GetVal(computer.position+2, computer)
	if val1 == val2 {
		group[2].SetVal(changePos, computer, 1)
	} else {
		group[2].SetVal(changePos, computer, 0)
	}
	computer.position += 4
}

func (computer *Computer) code9(group ModeGroup) {
	val := group[0].GetVal(computer.position+1, computer)
	computer.relativeBase += val
	computer.position += 2
}

func (computer *Computer) codeHalt() {
	computer.message("Halting!")
	computer.halted = true
}

func (computer Computer) message(msg string, params ...interface{}) {
	if !computer.silent {
		fmt.Printf("[%d]: ", computer.number)
		fmt.Printf(msg, params...)
		fmt.Println()
	}
}

func (computer Computer) close() {
	close(*computer.InChannel)
}

func (computer Computer) getInput() (input int) {
	//_, _ = fmt.Scanf("%d", &input)
	//return

	err := term.Init()
	if err != nil {
		panic(err)
	}
	defer term.Close()
	switch ev := term.PollEvent(); ev.Type {
	case term.EventKey:
		switch ev.Key {
		case term.KeyArrowLeft:
			term.Sync()
			return -1
		case term.KeyArrowRight:
			term.Sync()
			return 1
		}
	}

	return 0
}
