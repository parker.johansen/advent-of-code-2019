package intcode

import (
	"fmt"
	"strconv"
	"testing"
)

func TestNewComputer(t *testing.T) {
	number := 3
	phase := 7
	computer := NewComputer(number, phase, []int{})
	t.Run(fmt.Sprintf("Number should be %d", number), func(t *testing.T) {
		if computer.number != number {
			t.Errorf("expected: %d, actual: %d", number, computer.number)
		}
	})

	t.Run(fmt.Sprintf("Phase should be %d", phase), func(t *testing.T) {
		if computer.phase != phase {
			t.Errorf("expected: %d, actual: %d", phase, computer.phase)
		}
	})

	t.Run("Position should be 0", func(t *testing.T) {
		if computer.position != 0 {
			t.Errorf("expected: %d, actual: %d", 0, computer.position)
		}
	})

	t.Run("RelativeBase should be 0", func(t *testing.T) {
		if computer.relativeBase != 0 {
			t.Errorf("expected: %d, actual: %d", 0, computer.relativeBase)
		}
	})

	t.Run("PhaseSet should be false", func(t *testing.T) {
		if computer.phaseSet {
			t.Errorf("expected: %v, actual: %v", false, computer.phaseSet)
		}
	})

	t.Run("Halted should be false", func(t *testing.T) {
		if computer.halted {
			t.Errorf("expected: %v, actual: %v", false, computer.halted)
		}
	})

	t.Run("Channel should be empty", func(t *testing.T) {
		if channelLength := len(*computer.InChannel); channelLength != 0 {
			t.Errorf("expected length: 0, actual: %d", channelLength)
		}
	})

	t.Run("Next should be nil", func(t *testing.T) {
		if computer.OutChannel != nil {
			t.Errorf("expected: nil, actual: %v", computer.OutChannel)
		}
	})
}

func TestComputer_Run(t *testing.T) {
	var tests = []struct {
		input         []int
		expectedState []int
		expectedOut   []int
	}{
		// Day 05
		{
			[]int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50},
			[]int{3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50},
			nil,
		}, {
			[]int{1, 0, 0, 0, 99},
			[]int{2, 0, 0, 0, 99},
			nil,
		}, {
			[]int{2, 3, 0, 3, 99},
			[]int{2, 3, 0, 6, 99},
			nil,
		}, {
			[]int{2, 4, 4, 5, 99, 0},
			[]int{2, 4, 4, 5, 99, 9801},
			nil,
		}, {
			[]int{1, 1, 1, 4, 99, 5, 6, 0, 99},
			[]int{30, 1, 1, 4, 2, 5, 6, 0, 99},
			nil,
		},
		// Day 07
		// Day 09
		{
			[]int{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99},
			[]int{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99},
			[]int{109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99},
		},
		{
			[]int{104, 1125899906842624, 99},
			[]int{104, 1125899906842624, 99},
			[]int{1125899906842624},
		},
		{
			[]int{1102, 34915192, 34915192, 7, 4, 7, 99, 0},
			[]int{1102, 34915192, 34915192, 7, 4, 7, 99, 1219070632396864},
			[]int{1219070632396864},
		},
		// Day 11
		// Day 13
		// Day 15
		// Day 17
	}

	for i, test := range tests {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expected := test.expectedState
			computer := NewComputer(0, 0, test.input)
			outChannel := make(chan int, 1000)
			computer.setOutChannel(&outChannel)
			defer computer.close()

			err := computer.Run()
			close(outChannel)
			if err != nil {
				t.Error(err)
			}

			actual := computer.state

			for j, exp := range expected {
				if actual[j] != exp {
					t.Errorf("expected: %d, actual: %d", expected, actual)
				}
			}
			if test.expectedOut != nil {
				i := 0
				for out := range outChannel {
					if expectedOut := test.expectedOut[i]; out != expectedOut {
						t.Errorf("output expected: %d, actual: %d", expectedOut, out)
					}
					i++
				}
			}
		})
	}
}

func TestChange(t *testing.T) {
	var inputs = []struct {
		name                 string
		opcode               int
		phase                int
		state                []int
		modeGroup            ModeGroup
		expectedState        []int
		expectedRelativeBase int
	}{
		{
			"1 position",
			1,
			-1,
			[]int{1, 0, 0, 0, 99},
			ModeGroup{PositionMode, PositionMode, PositionMode},
			[]int{2, 0, 0, 0, 99},
			0,
		}, {
			"1 immediate",
			1,
			-1,
			[]int{1, 3, 30, 0, 99},
			ModeGroup{ImmediateMode, ImmediateMode, ImmediateMode},
			[]int{33, 3, 30, 0, 99},
			0,
		}, {
			"2 position",
			2,
			-1,
			[]int{2, 3, 0, 3, 99},
			ModeGroup{PositionMode, PositionMode, PositionMode},
			[]int{2, 3, 0, 6, 99},
			0,
		}, {
			"2 immediate",
			2,
			-1,
			[]int{2, 3, 33, 3, 99},
			ModeGroup{ImmediateMode, ImmediateMode, ImmediateMode},
			[]int{2, 3, 33, 99, 99},
			0,
		}, {
			"9 Immediate",
			9,
			0,
			[]int{9, 19, 99},
			ModeGroup{ImmediateMode, ImmediateMode, ImmediateMode},
			[]int{9, 19, 99},
			19,
		},
	}

	for _, input := range inputs {
		t.Run(input.name, func(t *testing.T) {
			expectedState := input.expectedState

			computer := Computer{
				0,
				input.phase,
				input.state,
				0,
				0,
				false,
				false,
				false,
				false,
				false,
				nil,
				nil,
				nil,
			}

			err := computer.change(Opcode{input.state[0], input.modeGroup})
			if err != nil {
				t.Error(err)
			}

			actual := computer.state

			for j, act := range actual {
				if act != expectedState[j] {
					t.Errorf("expected: %d, actual: %d", expectedState, actual)
				}
			}

			if computer.relativeBase != input.expectedRelativeBase {
				t.Errorf("expected relative base: %d, actual relative base: %d", input.expectedRelativeBase, computer.relativeBase)
			}
		})
	}
}
