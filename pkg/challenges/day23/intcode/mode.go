package intcode

type Mode int

type ModeGroup []Mode

const (
	PositionMode  Mode = iota
	ImmediateMode Mode = iota
	RelativeMode  Mode = iota
)

func (mode Mode) ToString() string {
	switch mode {
	case PositionMode:
		return "Position"
	case ImmediateMode:
		return "Immediate"
	case RelativeMode:
		return "Relative"
	default:
		return "Unknown"
	}
}

func GetMode(i int) Mode {
	switch i {
	case 0:
		return PositionMode
	case 1:
		return ImmediateMode
	case 2:
		return RelativeMode
	default:
		return ImmediateMode
	}
}

func (mode Mode) GetVal(position int, amplifier *Computer) int {
	switch mode {
	case PositionMode:
		pos := amplifier.state.GetValue(position)
		return amplifier.state.GetValue(pos)
	case ImmediateMode:
		return amplifier.state.GetValue(position)
	case RelativeMode:
		pos := amplifier.state.GetValue(position) + amplifier.relativeBase
		return amplifier.state.GetValue(pos)
	default:
		return PositionMode.GetVal(position, amplifier) // Default to position
	}
}

func (mode Mode) SetVal(position int, amplifier *Computer, val int) {
	switch mode {
	case PositionMode:
		fallthrough
	case ImmediateMode:
		amplifier.state.SetValue(position, val)
	case RelativeMode:
		amplifier.state.SetValue(position+amplifier.relativeBase, val)
	}
}
