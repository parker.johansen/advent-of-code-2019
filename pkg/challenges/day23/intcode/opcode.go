package intcode

import "strconv"

type Opcode struct {
	code      int
	modeGroup ModeGroup
}

func parseOpcode(input int) (opcode Opcode, err error) {
	inputStr := strconv.Itoa(input)
	inputLen := len(inputStr)
	for i := 0; i < 5-inputLen; i++ {
		inputStr = "0" + inputStr
	}

	var mode0, mode1, mode2 int
	mode2, err = strconv.Atoi(inputStr[0:1])
	if err != nil {
		return
	}
	mode1, err = strconv.Atoi(inputStr[1:2])
	if err != nil {
		return
	}
	mode0, err = strconv.Atoi(inputStr[2:3])
	if err != nil {
		return
	}

	code, err := strconv.Atoi(inputStr[3:5])
	if err != nil {
		return
	}

	opcode = Opcode{
		code,
		ModeGroup{
			GetMode(mode0),
			GetMode(mode1),
			GetMode(mode2),
		},
	}

	return
}
