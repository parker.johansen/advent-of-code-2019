package intcode

import "strconv"

type State []int

func NewState(stateStr []string) (state State, err error) {
	for _, str := range stateStr {
		var i int
		i, err = strconv.Atoi(str)
		if err != nil {
			return
		}
		state = append(state, i)
	}

	return
}

func (state *State) GetValue(position int) int {
	if position >= len(*state) {
		for i := len(*state); i <= position; i++ {
			*state = append(*state, 0)
		}
	}
	return (*state)[position]
}

func (state *State) SetValue(position int, value int) {
	if position >= len(*state) {
		for i := len(*state); i <= position; i++ {
			*state = append(*state, 0)
		}
	}
	(*state)[position] = value
}
