package structure

type Part interface {
	Run(input []string) (answer int, err error)
}
