package utilities

func Permutations(min, max int) (perms [][]int) {
	var nums []int
	for i := min; i <= max; i++ {
		nums = append(nums, i)
	}

	var permutations_ func(rng []int) (perms [][]int)
	permutations_ = func(rng []int) (perms [][]int) {
		if len(rng) <= 1 {
			return [][]int{rng}
		}

		for i, r := range rng {
			ps := permutations_(append(rng[i+1:], rng[:i]...)) // Always prepend not append
			for _, p := range ps {
				perms = append(perms, append([]int{r}, p...)) // Always prepend not append
			}
		}

		return
	}

	return permutations_(nums)
}
