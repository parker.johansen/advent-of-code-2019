package utilities

import (
	"fmt"
	"testing"
)

func TestPermutations(t *testing.T) {
	tests := []struct {
		min, max int
		expected [][]int
	}{
		{0, 1, [][]int{{0, 1}, {1, 0}}},
		{0, 2, [][]int{{0, 1, 2}, {0, 2, 1}, {1, 0, 2}, {1, 2, 0}, {2, 0, 1}, {2, 1, 0}}},
	}
	for _, test := range tests {
		t.Run(fmt.Sprintf("%d-%d", test.min, test.max), func(t *testing.T) {
			expected := test.expected
			var actual haystack = Permutations(test.min, test.max)
			for i := range expected {
				if !actual.contains(expected[i]) {
					t.Fatalf("expected: %v, actual: %v", expected, actual)
				}
			}
		})
	}
}

type needle []int
type haystack [][]int

func (lst haystack) contains(search needle) bool {
	for _, hay := range lst {
		equal := true
		for i := range hay {
			if hay[i] == search[i] {
				equal = false
			}
		}
		if equal {
			return true
		}
	}

	return false
}
